#include <iostream>
#include <any>
#include <vector>
#include <string>
#include <map>
#include <fstream>
#include <exception>

class JSON {
private:
    std::any data;
public:
    JSON(){
        data = nullptr;
    }

    JSON(const std::string &s, bool isFile) {
        if (isFile){
            parseFile(s);
        }else{
            parse(s,0);
        }
    }

    bool isArray() const {

    }

    bool isObject() const {

    }

    std::any &operator[](const std::string &key){

    }

    std::any &operator[](int index) {

    }

    static std::string parseString(const std::string &s, int &position) {
        std::string result;
        bool isQuotationMark = false;
        position += 1;
        for (; position < s.size(); position++) {
            if (s[position] != '"') {
                result += s[position];
            } else {
                isQuotationMark = true;
                return result;
            }
        }
        throw std::logic_error("Invalid input format 1\n");
    }
    
    static bool parseBool(const std::string& s, int& position){
        bool result = true;
        bool isBool = true;
        if (s[position] == 't') {
            position += 1;
            if (s[position] == 'r') {
                position += 1;
                if (s[position] == 'u') {
                    position += 1;
                    if (s[position] == 'e') {
                        result = true;
                    } else isBool = false;
                } else isBool = false;
            } else isBool = false;
        } else {
            position += 1;
            if (s[position] == 'a') {
                position += 1;
                if (s[position] == 'l') {
                    position += 1;
                    if (s[position] == 's') {
                        position += 1;
                        if (s[position] == 'e') {
                            result = false;
                        } else isBool = false;
                    } else isBool = false;
                } else isBool = false;
            } else isBool = false;
        }
        if (isBool) {
            return result;
        } else throw std::logic_error("Invalid input format 2\n");
    }

    static int parseNumber(const std::string& s, int& position){
        std::string number;
        number += s[position];
        position += 1;
        for (; position < s.size(); position++) {
            if (iswdigit(s[position])) {
                number += s[position];
            } else {
                if (s[position] == ' ' or s[position] == ',' or s[position] == ']' or s[position] == '}'){
                    position-=1;
                    return stoi(number);
                } else throw std::logic_error("Invalid input format 3\n");
            }
        }
    }

    static std::map<std::string, std::any> parseObject(const std::string &s, int &position) {
        std::map<std::string, std::any> result;
        std::any value;
        std::string key;
        int stateObject = 0;
        /* I know that it's very stupidly
         state = {
         0 - Looking for the beginning of a key or end;
         1 - Looking for a colon;
         2 - Looking for a value;
         3 - Looking for a comma or end;
         }*/
        position+=1;
        for (; position<s.size(); position++){
            if (s[position] == '"'){
                if (stateObject == 0){
                    key = parseString (s,position);
                    stateObject = 1;
                    continue;
                }
                if (stateObject == 2){
                    value = parseString (s,position);
                    result.insert(std::pair<std::string, std::any>(key,value));
                    stateObject = 3;
                    continue;
                } else throw std::logic_error("Invalid input format 4\n");
            }
            if (s[position] == ':'){
                if (stateObject == 1){
                    stateObject = 2;
                    continue;
                } else throw std::logic_error("Invalid input format 5\n");
            }
            if (s[position] == '}'){
                if (stateObject==0 or stateObject==3){
                    return result;
                } else throw std::logic_error("Invalid input format 6\n");
            }
            if (s[position] == ','){
                if (stateObject == 3){
                    stateObject = 0;
                    continue;
                } else throw std::logic_error("Invalid input format 7\n");
            }
            if (s[position] == '{'){
                if (stateObject == 2){
                    value = parseObject(s,position);
                    result.insert(std::pair<std::string, std::any>(key,value));
                    stateObject = 3;
                    continue;
                } else throw std::logic_error("Invalid input format 8\n");
            }
            if (s[position] == '['){
                if (stateObject == 2){
                    value = parseArray(s,position);
                    result.insert(std::pair<std::string, std::any>(key,value));
                    stateObject = 3;
                    continue;
                } else throw std::logic_error("Invalid input format 9\n");
            }
            if (s[position] == 't' or s[position] == 'f'){
                if (stateObject == 2){
                    value = parseBool(s,position);
                    result.insert(std::pair<std::string, std::any>(key,value));
                    stateObject = 3;
                    continue;
                } else throw std::logic_error("Invalid input format 10\n");
            }
            if (iswdigit(s[position])){
                if (stateObject == 2){
                    value = parseNumber(s,position);
                    result.insert(std::pair<std::string, std::any>(key,value));
                    stateObject = 3;
                    continue;
                } else throw std::logic_error("Invalid input format 11\n");
            }
        }
        throw std::logic_error("Invalid input format 12\n");
    }

    static std::vector<std::any> parseArray(const std::string& s, int& position){
        std::vector<std::any> result;
        std::any value;
        int stateArray = 0;
        /* I know that it's very stupidly
         state = {
         0 - Looking for a value or end;
         1 - Looking for a comma or end;
         }*/
        position+=1;
        for (;position<s.size(); position++){
            if (s[position] == '{'){
                if (stateArray == 0){
                    value = parseObject(s,position);
                    result.push_back(value);
                    stateArray=1;
                    continue;
                }else throw std::logic_error("Invalid input format 13\n");
            }
            if (s[position] == '['){
                if (stateArray == 0){
                    value = parseArray(s,position);
                    result.push_back(value);
                    stateArray=1;
                    continue;
                }else throw std::logic_error("Invalid input format 14\n");
            }
            if (s[position] == '"'){
                if (stateArray == 0){
                    value = parseString(s,position);
                    result.push_back(value);
                    stateArray=1;
                    continue;
                }else throw std::logic_error("Invalid input format 15\n");
            }
            if (s[position] == ','){
                if (stateArray == 1) {
                    stateArray=0;
                    continue;
                } else throw std::logic_error("Invalid input format 16\n");
            }
            if (iswdigit(s[position])){
                if (stateArray == 0) {
                    value = parseNumber(s, position);
                    result.push_back(value);
                    stateArray = 1;
                    continue;
                } else throw std::logic_error("Invalid input format 17\n");
            }
            if (s[position] == 't' or s[position] == 'f'){
                if (stateArray == 0){
                    value = parseBool(s,position);
                    result.push_back(value);
                    stateArray = 1;
                    continue;
                } else throw std::logic_error("Invalid input format 18\n");
            }
            if (s[position] == ']'){
                if (stateArray == 1 or stateArray == 0){
                    return result;
                }
            }
        }
        throw std::logic_error("Invalid input format 19\n");
    }

    void parse(const std::string &s, int position) {
        bool isJSON = false;
        for (; position < s.size(); position++) {
            if (s[position] == '{') {
                isJSON = true;
                data = parseObject(s,position);
                return;
            }
            if (s[position] == '[') {
                isJSON = true;
                data = parseArray(s,position);
                return;
            }
        }
        throw std::logic_error("Invalid input format 20\n");
    }

    void parseFile(const std::string &path) {
        std::ifstream JSON_file(path);
        std::string stroke, file;
        if (JSON_file.is_open()) {
            while (!JSON_file.eof()) {
                getline(JSON_file, stroke);
                file += stroke;
            }
            parse(file, 0);
            return;
        } else throw std::logic_error("File does not exist\n");
    }

};
int main(){
    JSON a;
    try {
        a.parse("{\"jjj\" :[1 ,2 , 3], \"key\" : true , \"gg\":{\"pp\": 12 }}", 0);
    }catch (std::logic_error &n) {
        std::cout << n.what();
        return 1;
    }
    return 0;
}
